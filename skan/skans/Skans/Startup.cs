﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Skans.Startup))]
namespace Skans
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
