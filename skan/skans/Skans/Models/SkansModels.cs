﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Skans.Models
{
    public class SkansModels
    {
    }

    public partial class User_Image
    {
        [Key]
        public int Id { get; set; }
        public int Inquiry_ID { get; set; }
        public string ImagePath { get; set; }
    }
    public class Inquiry
    {
        [Key]
        [Display(Name = "ID")]
        public int Inquiry_ID { get; set; }
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Display(Name = "Creation Time")]
        public DateTime CreationTime { get; set; }
       
        [Display(Name = "Status")]
        public string InquiryStatus_code { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }
        [Display(Name = "Skan id")]
        public string Skan_id { get; set; }
        [Display(Name = "Campus Code")]
        public string CityCampus { get; set; }
        [Display(Name = "Branch")]
        public string Branch { get; set; }
        [Display(Name = "Gender")]
        public string gender { get; set; }
        [Display(Name = "Qualification")]
        public string Qualification { get; set; }
        [Display(Name = "Date Of Inquiry")]
        public DateTime? DateOfInquiry { get; set; }
        [Display(Name = "Last Session")]
        public string lastSession{ get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [Display(Name = "Mobile No")]
        [DataType(DataType.PhoneNumber)]
        public string MobileNo { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "Inquirer Relation")]
        public string Relation { get; set; }
        [Display(Name = "Courses")]
        public string Course { get; set; }
        [Display(Name = "Other Course")]
        public string Others_Course { get; set; }
        [Display(Name = "Inquiry Mode")]

        public string Inquiry_Mod { get; set; }
        [Display(Name = "Inquiry Source ")]

        public string Info_Source { get; set; }
        [Display(Name = "Inquiry Type ")]

        public string Inquiry_Type { get; set; }
        [Display(Name = "Inquiry Status")]

        public string Inquiry_Status { get; set; }
        [Display(Name = "Cousre Enrolled")]

        public string Course_Enrolled { get; set; }
        [Display(Name = "Comments")]

        public string Comments { get; set; }
        [Display(Name = "Gift Code")]

        public int Giftcode { get; set; }
        public int Transferid { get; set; }
        public int[] Courseids { get; set; }
        public Inquiry()
                {
                     Courseids  = new int [50];
                }
        
    }

    public class FollowUpModel
    {
        [Key]
        public int FollowUpID { get; set; }
        
        [Display(Name = "Inquiry ID")]
        public int Inquiry_ID { get; set; }
        [Display(Name = "Follower Name")]
        public string UserName { get; set; }
        [Display(Name = "Inquirer Name")]
        public string InquirerName { get; set; }
        [Display(Name = "FollowUp Reason")]
        public string Comments { get; set; }
        
        [Display(Name = "FollowUp Status")]
        public string FollowUp_Status { get; set; }
        [Display(Name = "FollowUp Time")]
        public DateTime FollowUpTime { get; set; }
    }

}