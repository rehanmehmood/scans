﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Skans.Models
{
    public class SkansModel
    {
    }


    public class SkansContext : DbContext
    {

        public SkansContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer<SkansContext>(new DropCreateDatabaseIfModelChanges<SkansContext>());
        }
        public DbSet<Inquiry> Inquiries { get; set; }

    }


    public class Inquiry
    {
        [Key]
        public int Inquiry_ID { get; set; }

        public string Inquiry_Status;

        //[Range(0, 10000, ErrorMessage = "Please enter valid integer Number")]
        public string Inquiry_code { get; set; }
        //[RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Invalid Item Name")]
        public string First_Name { get; set; }
        //[RegularExpression(@"^[0-9]+$", ErrorMessage = "Invalid Quantity")]
        public string Last_Name { get; set; }
        //[RegularExpression(@"^[0-9]+$", ErrorMessage = "Invalid Items's Units")]
        public string Gender { get; set; }
        public string Remarks { get; set; }

        public DateTime? DateOfIn { get; set; }
        public string INFrom { get; set; }
        public string ReferenceOf { get; set; }
        public string VehicleNo { get; set; }
        public string InPreparedBy { get; set; }
        public string InApprovedBy { get; set; }
        public string InRecipient { get; set; }
    }
}