﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Skans.Models
{
    
    
    public class Registration
    {
        [Key]
        public int Inquiry_ID { get; set; }

        public string Course_Name { get; set; }
        public string Reg_type { get; set; }
        public string Entry_lvl { get; set; }
         [DataType(DataType.Text)]
        public string First_name {get;set;}
        public string Gender{get;set;}
        public DateTime DOB { get; set; }
        public string Sms_No {get; set;}
        public string Landline {get; set;}
        public string Mobile2 { get; set; }
        public string CNIC { get; set; }
        public string Other_identity  { get; set; }
        public string Email { get; set; }
        public string Com_Address { get; set; }
        public string Per_Address { get; set; }
        public string Comments { get; set; }
        public string Guardian_Name { get; set; }
        public string Relationship { get; set; }
        public string G_CNIC { get; set; }
        public string Occupation { get; set; }
        public string G_MobNo { get; set; }
        public string G_landline{ get; set; }
        public string G_OtherD{ get; set; }
        public string FD_reg { get; set; }
        public string ACCA_reg { get; set; }
        public string CIMA_reg { get; set; }
        public string CAT_reg { get; set; }
        public string ICAP_reg { get; set; }
        public string Pass_code { get; set; }
        public string GCU_No { get; set; }

        public string Last_School { get; set; }
        public string other_E_detail { get; set; }
        public List<EducationBackList> listofdata = new List<EducationBackList>();
       

    }
    public class EducationBackList 
    {
        [Key]
        public string Edu_ID { get; set; }
        public string School { get; set; }
        public string Board { get; set; }
        public string Qualification { get; set; }
        public string Grade { get; set; }
        public string Borad_RollNo { get; set; }
        public string Years { get; set; }
        public string Marks { get; set; }

    
    }
}