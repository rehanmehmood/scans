﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Skans.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Skans.Migrations.Configuration>("DefaultConnection"));
            
        }
        public DbSet<Qualification> Q_level { get; set; }
        public DbSet<Inquiry> Inquiries { get; set; }
        public DbSet<Inquiry_Mode> Mode { get; set; }
        public DbSet<Inquiry_Status> Status { get; set; }
        public DbSet<Inquiry_type> Type { get; set; }
        public DbSet<Course_enrolled> Enrolled { get; set; }
        public DbSet<Info_source> Source { get; set; }

        public DbSet<City> Cities { get; set; }
        public DbSet<Relation> Relations { get; set; }

        public DbSet<Skans.Models.CityCampus> CityCampus { get; set; }
        public DbSet<FollowUpModel> FollowUps { get; set; }
        public DbSet<Skans.Models.Branch> Branches { get; set; }
        public DbSet<Skans.Models.Course> Courses { get; set; }
        public DbSet<Skans.Models.Sub_Course> SubCourses { get; set; }

        public System.Data.Entity.DbSet<Skans.Models.Inquiry_Status_Code> Inquiry_Status_Code { get; set; }

        public System.Data.Entity.DbSet<Skans.Models.followup_status> followup_status { get; set; }
    }
}