﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Skans.Models
{
    public class Qualification
    {
        [Key]
        public int Qid { get; set; }
        public string level { get; set; }
    }

    public class Inquiry_Mode
    {
        [Key]
        public int Inid { get; set; }
        public string Mode { get; set; }
    }
    public class Inquiry_Status_Code
    {
        [Key]
        public int Statusid { get; set; }
        public string StatusCode { get; set; }
    }
    public class Inquiry_type
    {
        [Key]
        public int Intid { get; set; }
        public string Type { get; set; }
    }

    public class Info_source
    {
        [Key]
        public int Infoid { get; set; }
        public string Source { get; set; }
    }
    public class followup_status
    {
        [Key]
        public int id { get; set; }
        public string Status { get; set; }
    }

    public class Inquiry_Status    {
        [Key]
        public int InsId { get; set; }
        public string Status { get; set; }
    }
    public class Relation {
        [Key]
        public int Rid { get; set; }
        public string Name { get; set; }

    }
    public class Course_enrolled
    {
        [Key]
        public int Cid { get; set; }
        public string Enrolled { get; set; }

    }

    public class City
    {
         [Key]
        public int cityid { get; set; }
        public string Name { get; set; }
    
    }
    public class CityCampus
    {
        [Key]
        public int CityCampudID { get; set; }
        public string Ccode { get; set; }

    }
    public class Branch
    {
        [Key]
        public int BranchID { get; set; }
        public int Bcity { get; set; }
        public string Bcode { get; set; }

    }
    public class Course {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
    
    
    }
    public class Sub_Course
    {
     [Key]
        public int Sub_ID { get; set; }
        public string Course_ID { get; set; }
        public string Name { get; set; } 


    }
   
   

}