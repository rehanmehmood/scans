﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Skans.Models
{
    public class Inquiry_StatusController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Inquiry_Status
        public ActionResult Index()
        {
            return View(db.Status.ToList());
        }

        // GET: Inquiry_Status/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_Status inquiry_Status = db.Status.Find(id);
            if (inquiry_Status == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_Status);
        }

        // GET: Inquiry_Status/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inquiry_Status/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InsId,Status")] Inquiry_Status inquiry_Status)
        {
            if (ModelState.IsValid)
            {
                db.Status.Add(inquiry_Status);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inquiry_Status);
        }

        // GET: Inquiry_Status/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_Status inquiry_Status = db.Status.Find(id);
            if (inquiry_Status == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_Status);
        }

        // POST: Inquiry_Status/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InsId,Status")] Inquiry_Status inquiry_Status)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inquiry_Status).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inquiry_Status);
        }

        // GET: Inquiry_Status/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_Status inquiry_Status = db.Status.Find(id);
            if (inquiry_Status == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_Status);
        }

        // POST: Inquiry_Status/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inquiry_Status inquiry_Status = db.Status.Find(id);
            db.Status.Remove(inquiry_Status);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
