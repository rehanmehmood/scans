﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Skans.Models
{
    public class Sub_CourseController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Sub_Course
        public ActionResult Index()
        {
            return View(db.SubCourses.ToList());
        }

        // GET: Sub_Course/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sub_Course sub_Course = db.SubCourses.Find(id);
            if (sub_Course == null)
            {
                return HttpNotFound();
            }
            return View(sub_Course);
        }

        // GET: Sub_Course/Create
        public ActionResult Create()
        {
            ViewBag.Courses = new SelectList(db.Courses, "ID", "Name");
            return View();
        }

        // POST: Sub_Course/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Sub_ID,Course_ID,Name")] Sub_Course sub_Course)
        {
            if (ModelState.IsValid)
            {
                db.SubCourses.Add(sub_Course);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sub_Course);
        }

        // GET: Sub_Course/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sub_Course sub_Course = db.SubCourses.Find(id);
            if (sub_Course == null)
            {
                return HttpNotFound();
            }
            return View(sub_Course);
        }

        // POST: Sub_Course/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Sub_ID,Course_ID,Name")] Sub_Course sub_Course)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sub_Course).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sub_Course);
        }

        // GET: Sub_Course/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sub_Course sub_Course = db.SubCourses.Find(id);
            if (sub_Course == null)
            {
                return HttpNotFound();
            }
            return View(sub_Course);
        }

        // POST: Sub_Course/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sub_Course sub_Course = db.SubCourses.Find(id);
            db.SubCourses.Remove(sub_Course);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
