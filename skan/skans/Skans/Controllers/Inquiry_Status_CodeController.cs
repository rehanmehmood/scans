﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Skans.Models;

namespace Skans.Controllers
{
    public class Inquiry_Status_CodeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Inquiry_Status_Code
        public ActionResult Index()
        {
            return View(db.Inquiry_Status_Code.ToList());
        }

        // GET: Inquiry_Status_Code/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_Status_Code inquiry_Status_Code = db.Inquiry_Status_Code.Find(id);
            if (inquiry_Status_Code == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_Status_Code);
        }

        // GET: Inquiry_Status_Code/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inquiry_Status_Code/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Statusid,StatusCode")] Inquiry_Status_Code inquiry_Status_Code)
        {
            if (ModelState.IsValid)
            {
                db.Inquiry_Status_Code.Add(inquiry_Status_Code);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inquiry_Status_Code);
        }

        // GET: Inquiry_Status_Code/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_Status_Code inquiry_Status_Code = db.Inquiry_Status_Code.Find(id);
            if (inquiry_Status_Code == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_Status_Code);
        }

        // POST: Inquiry_Status_Code/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Statusid,StatusCode")] Inquiry_Status_Code inquiry_Status_Code)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inquiry_Status_Code).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inquiry_Status_Code);
        }

        // GET: Inquiry_Status_Code/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_Status_Code inquiry_Status_Code = db.Inquiry_Status_Code.Find(id);
            if (inquiry_Status_Code == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_Status_Code);
        }

        // POST: Inquiry_Status_Code/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inquiry_Status_Code inquiry_Status_Code = db.Inquiry_Status_Code.Find(id);
            db.Inquiry_Status_Code.Remove(inquiry_Status_Code);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
