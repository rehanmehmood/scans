﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Skans.Models
{
    public class CityCampusController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CityCampus
        public ActionResult Index()
        {
            return View(db.CityCampus.ToList());
        }

        // GET: CityCampus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityCampus cityCampus = db.CityCampus.Find(id);
            if (cityCampus == null)
            {
                return HttpNotFound();
            }
            return View(cityCampus);
        }

        // GET: CityCampus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CityCampus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CityCampudID,Ccode")] CityCampus cityCampus)
        {
            if (ModelState.IsValid)
            {
                db.CityCampus.Add(cityCampus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cityCampus);
        }

        // GET: CityCampus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityCampus cityCampus = db.CityCampus.Find(id);
            if (cityCampus == null)
            {
                return HttpNotFound();
            }
            return View(cityCampus);
        }

        // POST: CityCampus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CityCampudID,Ccode")] CityCampus cityCampus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cityCampus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cityCampus);
        }

        // GET: CityCampus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityCampus cityCampus = db.CityCampus.Find(id);
            if (cityCampus == null)
            {
                return HttpNotFound();
            }
            return View(cityCampus);
        }

        // POST: CityCampus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CityCampus cityCampus = db.CityCampus.Find(id);
            db.CityCampus.Remove(cityCampus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
