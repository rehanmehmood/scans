﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Skans.Models
{
    public class Info_sourceController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Info_source
        public ActionResult Index()
        {
            return View(db.Source.ToList());
        }

        // GET: Info_source/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Info_source info_source = db.Source.Find(id);
            if (info_source == null)
            {
                return HttpNotFound();
            }
            return View(info_source);
        }

        // GET: Info_source/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Info_source/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Infoid,Source")] Info_source info_source)
        {
            if (ModelState.IsValid)
            {
                db.Source.Add(info_source);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(info_source);
        }

        // GET: Info_source/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Info_source info_source = db.Source.Find(id);
            if (info_source == null)
            {
                return HttpNotFound();
            }
            return View(info_source);
        }

        // POST: Info_source/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Infoid,Source")] Info_source info_source)
        {
            if (ModelState.IsValid)
            {
                db.Entry(info_source).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(info_source);
        }

        // GET: Info_source/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Info_source info_source = db.Source.Find(id);
            if (info_source == null)
            {
                return HttpNotFound();
            }
            return View(info_source);
        }

        // POST: Info_source/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Info_source info_source = db.Source.Find(id);
            db.Source.Remove(info_source);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
