﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Skans.Models
{
    public class Inquiry_typeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Inquiry_type
        public ActionResult Index()
        {
            return View(db.Type.ToList());
        }

        // GET: Inquiry_type/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_type inquiry_type = db.Type.Find(id);
            if (inquiry_type == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_type);
        }

        // GET: Inquiry_type/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inquiry_type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Intid,Type")] Inquiry_type inquiry_type)
        {
            if (ModelState.IsValid)
            {
                db.Type.Add(inquiry_type);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inquiry_type);
        }

        // GET: Inquiry_type/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_type inquiry_type = db.Type.Find(id);
            if (inquiry_type == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_type);
        }

        // POST: Inquiry_type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Intid,Type")] Inquiry_type inquiry_type)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inquiry_type).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inquiry_type);
        }

        // GET: Inquiry_type/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry_type inquiry_type = db.Type.Find(id);
            if (inquiry_type == null)
            {
                return HttpNotFound();
            }
            return View(inquiry_type);
        }

        // POST: Inquiry_type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inquiry_type inquiry_type = db.Type.Find(id);
            db.Type.Remove(inquiry_type);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
