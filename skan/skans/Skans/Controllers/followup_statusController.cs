﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Skans.Models;

namespace Skans.Controllers
{
    public class followup_statusController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: followup_status
        public ActionResult Index()
        {
            return View(db.followup_status.ToList());
        }

        // GET: followup_status/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            followup_status followup_status = db.followup_status.Find(id);
            if (followup_status == null)
            {
                return HttpNotFound();
            }
            return View(followup_status);
        }

        // GET: followup_status/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: followup_status/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Status")] followup_status followup_status)
        {
            if (ModelState.IsValid)
            {
                db.followup_status.Add(followup_status);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(followup_status);
        }

        // GET: followup_status/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            followup_status followup_status = db.followup_status.Find(id);
            if (followup_status == null)
            {
                return HttpNotFound();
            }
            return View(followup_status);
        }

        // POST: followup_status/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Status")] followup_status followup_status)
        {
            if (ModelState.IsValid)
            {
                db.Entry(followup_status).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(followup_status);
        }

        // GET: followup_status/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            followup_status followup_status = db.followup_status.Find(id);
            if (followup_status == null)
            {
                return HttpNotFound();
            }
            return View(followup_status);
        }

        // POST: followup_status/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            followup_status followup_status = db.followup_status.Find(id);
            db.followup_status.Remove(followup_status);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
