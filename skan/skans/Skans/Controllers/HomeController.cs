﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Skans.Models;
using System.Net;
using System.Data.Entity;
using PagedList;
using System.IO;
using System.Text;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace Skans.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public Random rand = new Random();
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult About()
        {
            //= "Your description page.";

            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult Contact()
        {
            //string userId = this.User.Identity.GetUserId();
            //string username = this.User.Identity.Name;
            //ViewBag.Message = userId;
            //ViewBag.User = username;

            return View();
        }
        [Authorize]
        public ActionResult Inquiry()
        {


            ViewBag.Q_list = new SelectList(db.Q_level, "level", "level");
            ViewBag.Mode = new SelectList(db.Mode, "Mode", "Mode");
            ViewBag.Type = new SelectList(db.Type, "Type", "Type");
            ViewBag.Source = new SelectList(db.Source, "Source", "Source");
            ViewBag.Status = new SelectList(db.Status, "Status", "Status");
            ViewBag.Enrolled = new SelectList(db.Enrolled, "Enrolled", "Enrolled");
            ViewBag.Cities = new SelectList(db.Cities, "Name", "Name");
            ViewBag.Relations = new SelectList(db.Relations, "Name", "Name");
            ViewBag.CityCampus = new SelectList(db.CityCampus, "Ccode", "Ccode");
            ViewBag.Branch = new SelectList(db.Branches, "Bcode", "Bcode");
            var SUBC = db.SubCourses.Select(c => new
            {
                SID = c.Sub_ID,
                SName = c.Name
            }).ToList();
            ViewBag.Subcourses = new MultiSelectList(SUBC, "SID", "SName");

            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult Inquiry(Inquiry obj)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string str = "";
            //////////////////////////////
            int digitcode = rand.Next(0, 100000);
            if(digitcode<10000)
            {
            while(digitcode<10000)
            { digitcode += 498; }
            }
            if (digitcode >= 100000)
            {
                while (digitcode >= 100000)
                { digitcode -= 497; }
            }
            obj.Giftcode = digitcode;
            ///////////////////////////////////////
            obj.InquiryStatus_code = "Open";
            foreach (int value in obj.Courseids)
            {
                str = str + value + ",";
            }
            obj.Course = str;
            DateTime date = obj.DOB;

            obj.DOB = date.Date;
            /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

            string userId = User.Identity.GetUserId();

            ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == userId);
            //string userId = this.User.Identity.GetUserId();
            string username = User.Identity.Name;
            obj.UserName = username;
            obj.CreationTime = DateTime.Now;

            /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
            if (ModelState.IsValid)
            {
                db.Inquiries.Add(obj);
                db.SaveChanges();

                GenSMS(obj);
                return RedirectToAction("Details", "Home", new { id = obj.Inquiry_ID });

            }

            return View(obj);
        }
      
       
        [HttpGet]
        [Authorize]
        public ActionResult Edit(int? id)
        {

           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry Record = db.Inquiries.Find(id);
            if ((Record == null))
            {
                return HttpNotFound();
            }
            else
            {
                DateTime Editime = DateTime.Now;
                if (Editime.Date == Record.CreationTime.Date && Record.UserName == User.Identity.GetUserName())
                {
                    char[] delimiterChars = { ',' };

                    string text = Record.Course;


                    string[] words = text.Split(delimiterChars);
                    int i = 0;
                    foreach (string s in words)
                    {
                        if (s.Any() && s.All(Char.IsDigit))
                        {
                            Record.Courseids[i] = Convert.ToInt32(s);
                            i++;
                        }
                    }
                   
                    ViewBag.Q_list = new SelectList(db.Q_level, "level", "level");
                    ViewBag.Mode = new SelectList(db.Mode, "Mode", "Mode");
                    ViewBag.Type = new SelectList(db.Type, "Type", "Type");
                    ViewBag.Source = new SelectList(db.Source, "Source", "Source");
                    ViewBag.Status = new SelectList(db.Status, "Status", "Status");
                    ViewBag.Enrolled = new SelectList(db.Enrolled, "Enrolled", "Enrolled");
                    ViewBag.Cities = new SelectList(db.Cities, "Name", "Name");
                    ViewBag.Relations = new SelectList(db.Relations, "Name", "Name");
                    ViewBag.CityEdit = new SelectList(db.CityCampus, "Ccode", "Ccode");
                    ViewBag.Bran = new SelectList(db.Branches, "Bcode", "Bcode");
                    var SUBC = db.SubCourses.Select(c => new
                    {
                        SID = c.Sub_ID,
                        SName = c.Name
                    }).ToList();
                    ViewBag.Subcourses = new MultiSelectList(SUBC, "SID", "SName");

                    return View(Record);

                }
                else
                {
                    int Olderid = Record.Inquiry_ID;
                    Record.InquiryStatus_code = "Open";
                    Record.CreationTime = DateTime.Now;
                    Record.DateOfInquiry = DateTime.Now;
                    //////////////////////////////
                    int digitcode = rand.Next(0, 100000);
                    if (digitcode < 10000)
                    {
                        while (digitcode < 10000)
                        { digitcode += 498; }
                    }
                    if (digitcode >= 100000)
                    {
                        while (digitcode >= 100000)
                        { digitcode -= 497; }
                    }
                    Record.Giftcode = digitcode;
                    ///////////////////////////////////////
                    Record.UserName = User.Identity.GetUserName();
                    if (ModelState.IsValid)
                    {
                        db.Inquiries.Add(Record);
                        db.SaveChanges();
                        db.Entry(Record).GetDatabaseValues();
                        GenSMS(Record);
                    }
                    Inquiry old = db.Inquiries.Find(Olderid);
                    if (old != null)
                    {
                        old.InquiryStatus_code = "Transferred";
                        old.Transferid = Record.Inquiry_ID;
                        if (ModelState.IsValid)
                        {

                            try
                            {
                                db.Entry(old).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            catch (DbUpdateConcurrencyException ex)
                            {
                                ViewBag.error = "old transaction not modified" + old.Inquiry_ID;
                            }


                            return RedirectToAction("Details", "Home", new { id = Record.Inquiry_ID });

                        }
                    }
                    return RedirectToAction("Details", "Home", new { id = Record.Inquiry_ID });
                }
            }
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Inquiry Record)
        {
            string str = "";
            if (Record.Courseids != null)
            {

                foreach (int value in Record.Courseids)
                {
                    str = str + value + ",";
                }
                Record.Course = str;
            }


            if (ModelState.IsValid)
            {

                try
                {
                    db.Entry(Record).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Details", "Home", new { id = Record.Inquiry_ID });
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return RedirectToAction("Details", "Home", new { id = Record.Inquiry_ID });
                }
            }
            char[] delimiterChars = { ',' };

            string text = Record.Course;


            string[] words = text.Split(delimiterChars);
            int i = 0;
            foreach (string s in words)
            {
                if (s.Any() && s.All(Char.IsDigit))
                {
                    Record.Courseids[i] = Convert.ToInt32(s);
                    i++;
                }
            }
            //string str = "";
            //if (Record.Courseids != null)
            //{

            //    foreach (int value in Record.Courseids)
            //    {
            //        str = str + value + ",";
            //    }
            //    Record.Course = str;
            //}
            //DateTime date = Record.DOB;

            //Record.DOB = date.Date;
            ViewBag.Q_list = new SelectList(db.Q_level, "level", "level");
            ViewBag.Mode = new SelectList(db.Mode, "Mode", "Mode");
            ViewBag.Type = new SelectList(db.Type, "Type", "Type");
            ViewBag.Source = new SelectList(db.Source, "Source", "Source");
            ViewBag.Status = new SelectList(db.Status, "Status", "Status");
            ViewBag.Enrolled = new SelectList(db.Enrolled, "Enrolled", "Enrolled");
            ViewBag.Cities = new SelectList(db.Cities, "Name", "Name");
            ViewBag.Relations = new SelectList(db.Relations, "Name", "Name");
            ViewBag.CityEdit = new SelectList(db.CityCampus, "Ccode", "Ccode");
            ViewBag.Bran = new SelectList(db.Branches, "Bcode", "Bcode");
            var SUBC = db.SubCourses.Select(c => new
            {
                SID = c.Sub_ID,
                SName = c.Name
            }).ToList();
            ViewBag.Subcourses = new MultiSelectList(SUBC, "SID", "SName");
            return View(Record);
        }




        [Authorize]
        public ActionResult Details(int? id)
        {
            ViewBag.hide = "false";
            if (TempData["fail"] != null)
            {
                ViewBag.fail = TempData["fail"];
            }
            if (TempData["avoidHide"] != null)
            {
                ViewBag.fail = TempData["fail"];
                ViewBag.hideAvoid = TempData["avoidHide"];
                ViewBag.hide = "true";
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry inquiry1 = db.Inquiries.Find(id);
            if (inquiry1 == null)
            {
                return HttpNotFound();
            }
            if (inquiry1.InquiryStatus_code == "Verified")
            {
                ViewBag.hide = "true";

            }
            if (inquiry1.InquiryStatus_code == "Void" || inquiry1.InquiryStatus_code == "Transferred")
            {
                ViewBag.hide = "true";
                ViewBag.hideAvoid = "true";

            }
            char[] delimiterChars = { ',' };

            string text = inquiry1.Course;


            string[] words = text.Split(delimiterChars);

            int courseid;
            string IdstoString = "";
            foreach (string s in words)
            {
                if (s.Any() && s.All(Char.IsDigit))
                {
                    courseid = Convert.ToInt32(s);
                    Sub_Course list = db.SubCourses.Find(courseid);
                    IdstoString = IdstoString + list.Name + ", ";

                }
            }
            inquiry1.Course = IdstoString;
            return View(inquiry1);
        }

        public bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
        [Authorize]
        public ActionResult ShowList(int? id, string sortOrder, string searchString, string option, string currentFilter, int? page)
        {
            int pageSize = 15;
            int pageNumber = (page ?? 1);

            if (id != null)
            { 
                ViewBag.highlight = id;
            }
            /*-------------------------------------------------*/
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var inquiries = from s in db.Inquiries
                            select s;


            if (option == "FirstName")
            {
                inquiries = inquiries.Where(s => s.FirstName.ToUpper().Contains(searchString.ToUpper()));
                // return View(inquiries.ToList());
            }
            else if (option == "LastName")
            {
                inquiries = inquiries.Where(s => s.LastName.ToUpper().Contains(searchString.ToUpper()));
                //return View(inquiries.ToList());
            }
            else if (option == "ID")//&& IsDigitsOnly(searchString.ToString())
            {
                try
                {
                    int inq_id = Convert.ToInt32(searchString.ToString());
                    inquiries = inquiries.Where(s => s.Inquiry_ID == inq_id);
                }
                catch 
                {
                    inquiries = null;
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    return View(inquiries.ToPagedList(pageNumber,pageSize));
                }
                
                //return View(inquiries.ToList());
                
            }
 
            else if (option == "status")
            {
                inquiries = inquiries.Where(s => s.InquiryStatus_code.Contains(searchString.ToUpper()));
                //return View(inquiries.ToList());

            }
            switch (sortOrder)
            {
                case "name_desc":
                    inquiries = inquiries.OrderByDescending(s => s.LastName);
                    break;
                default: // Name ascending
                    inquiries = inquiries.OrderBy(s => s.Inquiry_ID);
                    break;
            }

            
            return View(inquiries.ToPagedList(pageNumber, pageSize));
            /*-------------------------------------------------------------------*/


        }



          [Authorize]
          [HttpGet]
          public ActionResult FollowUp(int? id)
          {
              if (id == null)
              {
                  return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
              }
              else
              {
                  Inquiry followup = db.Inquiries.Find(id);
                  if (followup == null)
                  {
                      return HttpNotFound();
                  }
                  //if (followup.InquiryStatus_code == "Void" || followup.InquiryStatus_code == "Transferred")
                  //{
                  //    TempData["FollowHide"] = "true";

                  //}
                  ViewBag.Status = new SelectList(db.followup_status, "Status", "Status");
                  FollowUpModel Inq_followup = new FollowUpModel();

                  Inq_followup.Inquiry_ID = followup.Inquiry_ID;
                  Inq_followup.InquirerName = followup.FirstName + " " + followup.LastName;
                  Inq_followup.FollowUp_Status = followup.Inquiry_Status;
                  //Inq_followup.UserName = followup.UserName;

                  return View(Inq_followup);
              }
          }
          [Authorize]
          [HttpPost]
          [ValidateAntiForgeryToken]
          public ActionResult FollowUp(FollowUpModel follow)
          {

              if (follow == null)
              {
                  return HttpNotFound();
              }

              else
              {
                  string followup_status = follow.FollowUp_Status.ToString();

                  string reason = follow.Comments.ToString();
                  follow.FollowUpTime = DateTime.Now;


                  var errors = ModelState.Values.SelectMany(v => v.Errors);

                  if (ModelState.IsValid)
                  {
                      try
                      {
                          follow.UserName = User.Identity.Name;
                          follow.Comments = reason;
                          db.FollowUps.Add(follow);
                          //db.Entry(follow).State = EntityState.Modified;
                          db.SaveChanges();
                          if (follow.FollowUp_Status == "Void")
                          {
                              Inquiry obj = db.Inquiries.Find(follow.Inquiry_ID);
                              if (obj != null)
                              {
                                  obj.InquiryStatus_code = "Void";
                                  db.Entry(obj).State = EntityState.Modified;
                                  db.SaveChanges();
                                  TempData["AvoidHide"] = "true";
                              }


                          }
                          if (follow.FollowUp_Status == "Open")
                          {
                              Inquiry obj = db.Inquiries.Find(follow.Inquiry_ID);
                              if (obj != null)
                              {
                                  obj.InquiryStatus_code = "Open";
                                  db.Entry(obj).State = EntityState.Modified;
                                  follow.Comments = User.Identity.Name + " Opened it";
                                  db.FollowUps.Add(follow);
                                  db.SaveChanges();


                              }

                          }


                      }
                      catch (DbUpdateConcurrencyException ex)
                      {
                          return RedirectToAction("Details", "Home", new { id = follow.Inquiry_ID });
                      }
                  }

                  return RedirectToAction("FollowUp", "Home", new { id = follow.Inquiry_ID });
              }

          }

          public ActionResult FollowUpList(int? id)
          {
              //int  ID = Convert.ToInt32(TempData["Inq_ID"].ToString());
              if (id != null)
              {
                  IEnumerable<FollowUpModel> FollowupList = new List<FollowUpModel>();

                  using (var context = new ApplicationDbContext())
                  {
                      FollowupList = (from u in context.FollowUps
                                      orderby u.FollowUpID descending
                                      where u.Inquiry_ID == id
                                      select u).ToList();
                  }

                  return View(FollowupList);
              }
              else
              {
                  return View();
              }
          }



        [Authorize]
        [HttpPost]
        public ActionResult Verify(FormCollection form)
        {
            int obj;
            try
            {
                obj = Convert.ToInt32(form["id"].ToString());
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry inquiry1 = db.Inquiries.Find(obj);
            if (inquiry1 == null)
            {
                return HttpNotFound();
            }

            else
            {
                int code = Convert.ToInt32(form["code"].ToString());
                if (inquiry1.Giftcode == code)
                {
                    inquiry1.InquiryStatus_code = "Verified";
                    if (ModelState.IsValid)
                    {
                        db.Entry(inquiry1).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("ShowList", "Home", new { id = inquiry1.Inquiry_ID });
                    }
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["fail"] = "true";
                    return RedirectToAction("Details", "Home", new { id = obj });
                }
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult Resend(FormCollection form)
        {
            int obj;
            try
            {
                obj = Convert.ToInt32(form["Rid"].ToString());
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry inquiry1 = db.Inquiries.Find(obj);
            if (inquiry1 == null)
            {
                return HttpNotFound();
            }

            else
            {
                string Mob = form["MobileNo"].ToString();
                if (Mob == inquiry1.MobileNo)
                {
                    // GenSMS(inquiry1);

                }
                else
                {
                    inquiry1.MobileNo = Mob;
                    if (ModelState.IsValid)
                    {

                        try
                        {
                            db.Entry(inquiry1).State = EntityState.Modified;
                            db.SaveChanges();
                            //GenSMS(inquiry1);
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            return RedirectToAction("ShowList", "Home", new { id = inquiry1.Inquiry_ID });
                        }


                        return RedirectToAction("Details", "Home", new { id = inquiry1.Inquiry_ID });

                    }

                }
            }
            return RedirectToAction("Details", "Home", new { id = inquiry1.Inquiry_ID });
        }
        [Authorize]
        public ActionResult Avoid(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inquiry inquiry1 = db.Inquiries.Find(id);
            if (inquiry1 == null)
            {
                return HttpNotFound();
            }
            else
            {
                inquiry1.InquiryStatus_code = "Avoid";
                if (ModelState.IsValid)
                {

                    try
                    {
                        db.Entry(inquiry1).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        return RedirectToAction("ShowList", "Home", new { id = inquiry1.Inquiry_ID });
                    }




                }
                TempData["AvoidHide"] = "true";
                return RedirectToAction("Details", "Home", new { id = inquiry1.Inquiry_ID });
            }

        }
        [Authorize]
        public String GenSMS(Inquiry obj)
        {


            string Text = readHtmlPage("http://www.sms4connect.com/api/sendsms.php/sendsms/url", obj);
            ViewBag.response = Text;


            //return RedirectToAction("Error", "Home");
            return Text;
        }
        private String readHtmlPage(string url, Inquiry no)
        {
            String result = "";
            string number = no.MobileNo.ToString();
            string inq_ID = no.Inquiry_ID.ToString();
            string inq_status = no.Giftcode.ToString();
            string NoIWant = number.Substring(1);
            string msg = "Welcome Note From Skans \n your Skans ID is:" + inq_ID + "\n Your Gift Code is:" + inq_status;

            String message = HttpUtility.UrlEncode(" " + msg + " \n \n (c !~`@#$%^&*)SKANS(&)2016");
            String strPost = "id=923214017228&pass=hger5522&msg=" + message + "&to=92" + NoIWant + "&mask=SKANS&type=xml&lang=English";

            StreamWriter myWriter = null;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);

            objRequest.Method = "POST";
            objRequest.ContentLength = Encoding.UTF8.GetByteCount(strPost);
            objRequest.ContentType = "application/x-www-form-urlencoded";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                myWriter.Close();
            }
            HttpWebResponse objResponse =
            (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr = new
            StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                // Close and clean up the StreamReader
                sr.Close();
            }

            return result;
        }
      
    }
}