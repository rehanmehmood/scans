﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Skans.Models
{
    public class Course_enrolledController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Course_enrolled
        public ActionResult Index()
        {
            return View(db.Enrolled.ToList());
        }

        // GET: Course_enrolled/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course_enrolled course_enrolled = db.Enrolled.Find(id);
            if (course_enrolled == null)
            {
                return HttpNotFound();
            }
            return View(course_enrolled);
        }

        // GET: Course_enrolled/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Course_enrolled/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Cid,Enrolled")] Course_enrolled course_enrolled)
        {
            if (ModelState.IsValid)
            {
                db.Enrolled.Add(course_enrolled);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(course_enrolled);
        }

        // GET: Course_enrolled/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course_enrolled course_enrolled = db.Enrolled.Find(id);
            if (course_enrolled == null)
            {
                return HttpNotFound();
            }
            return View(course_enrolled);
        }

        // POST: Course_enrolled/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Cid,Enrolled")] Course_enrolled course_enrolled)
        {
            if (ModelState.IsValid)
            {
                db.Entry(course_enrolled).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(course_enrolled);
        }

        // GET: Course_enrolled/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course_enrolled course_enrolled = db.Enrolled.Find(id);
            if (course_enrolled == null)
            {
                return HttpNotFound();
            }
            return View(course_enrolled);
        }

        // POST: Course_enrolled/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Course_enrolled course_enrolled = db.Enrolled.Find(id);
            db.Enrolled.Remove(course_enrolled);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
